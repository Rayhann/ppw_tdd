from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, doing_some, jsonlibrary, subs
from .models import Do, Subscriber
from .forms import Status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
import time


# Create your tests here.
class Lab6Test(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)


    def test_url_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)
    
    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_model_can_create_new_status(self):
        # Creating a new activity
        new_activity = Do.objects.create(status='Apa yang anda pikirkan?')
    
        # Retrieving all available activity
        counting_all_available = Do.objects.all().count()
        self.assertEqual(counting_all_available, 1)
    
    def test_form_input_has_placeholder_and_css_classes(self):
        form = Status_Form()
        self.assertIn('class="inputan"', form.as_p())
        self.assertIn('id="id_status"', form.as_p())
    
    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
        form.errors['status'],
        ["This field is required."]
        )

    def test_lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/doing_some/', {'status': test})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
    
    def test_lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/doing_some/', {'status': ''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)


#Challenge
    def test_url_Challenge_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code, 200)

    def test_using_template(self):
        response = Client().get('/profile')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_page(self):
        response = Client().get('/profile')
        html = response.content.decode('utf8')
        self.assertIn('Daffa Muhammad Rayhan', html)
        self.assertIn('28 Agustus 1999',html)
        self.assertIn('humoris',html)

#Story 9
    def test_url(self):
        response=Client().get('/book')
        self.assertEqual(response.status_code, 200)
    def test_url_book(self):
        response = Client().get('/jsonlibrary')
        self.assertEqual(response.status_code, 200)
    def test_view_book(self):
        found = resolve('/jsonlibrary')
        self.assertEqual(found.func, jsonlibrary)
    def json_data(self):
        response = Client().get('/jsonlibrary').json()
        data = response['data']['kinds']
        self.assertIn('books#volumes', data)

#Stroy 10
    def test_url_subs(self):
        response = Client().get('/subs')
        self.assertEqual(response.status_code, 200)
    def test_function_subs(self):
        found = resolve('/subs')
        self.assertEqual(found.func, subs)
    def model_can_create_new_subs(self):
        Subscriber.objects.create(name='apapa', email='apa@gmail.com', password='apakek')
        count = Subscriber.objects.all().count()
        self.assertEqual(count, 1)
    def test_model_can_create_new_subs(self):
        new_subs = Subscriber.objects.create(name='apakek', email='kek@gmail.com', password='kol')
        counting_all_available = Subscriber.objects.all().count()
        self.assertEqual(counting_all_available, 1)


#Functional Test 
class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        time.sleep(5)
        # find the form element
        status = selenium.find_element_by_id('id_status')
        submit = selenium.find_element_by_id('kirim')

        # Fill the form with data
        status.send_keys('coba coba')

        # submitting the form
        submit.send_keys(Keys.RETURN)
        time.sleep(3)
        self.assertIn('coba coba', self.selenium.page_source)

    def test_accordion(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/') 
        accr = selenium.page_source
        self.assertIn("Aktifitas saya saat ini", accr)

    # def test_changeTheme(self):
    #     selenium = self.selenium
    #     selenium.get('http://127.0.0.1:8000/profile')
    #     time.sleep(5)
    #     # gelap = selenium.find_element_by_id('item')
    #     # gelap.click()
    #     button = selenium.find_element_by_id('button')
    #     button.click()
    #     terang = selenium.find_element_by_id('cerah')
    #     terang.click()
    #     background = self.selenium.find_element_by_tag_name("body").value_of_css_property("background-color")
    #     self.assertEqual("rgba(255, 255, 255, 1)", background)
    #     time.sleep(5)
#Challenge Story 7
    
    #Test to find there is title
    # def test_text_in_homepage(self):
    #     selenium = self.selenium
    #     selenium.get('http://rayhanapp.herokuapp.com/mysite/Test')
    #     title = selenium.find_element_by_tag_name('title')
    #     self.assertIn("My Website", self.selenium.page_source)

    # #Test to find text
    # def test_text_in_homepage2(self):
    #     selenium = self.selenium
    #     selenium.get('http://rayhanapp.herokuapp.com/mysite/Test')
    #     body = selenium.find_element_by_id('tt')
    #     self.assertIn("Terima kasih telah mengunjungi situs saya", self.selenium.page_source)

    # #Test submit button (bisa atau tidak)
    # def test_submit_button_on_form(self):
    #     selenium = self.selenium
    #     selenium.get('http://rayhanapp.herokuapp.com/mysite/bukaForm')
    #     submit = selenium.find_element_by_tag_name('button')
    #     submit.send_keys(Keys.RETURN)

    # #Test css in homepage
    # def test_homepage_background(self):
    #     selenium = self.selenium
    #     selenium.get('http://rayhanapp.herokuapp.com/mysite/Test')
    #     body = selenium.find_element_by_tag_name('nav')
    #     body.value_of_css_property("background-color:black")

    # #Test css on form page
    # def test_margin_on_page_form_date(self):
    #     selenium = self.selenium
    #     selenium.get('http://rayhanapp.herokuapp.com/mysite/bukaForm')
    #     body = selenium.find_element_by_id('id_date')
    #     body.value_of_css_property("margin-bottom:3vw")