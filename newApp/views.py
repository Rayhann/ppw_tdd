from django.contrib import auth
from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .forms import Status_Form, SubscriberForm
from .models import Do, Subscriber
import requests,json


# Create your views here.
response = {}
def index(request):    
    response['author'] = "" #TODO Implement yourname
    todo = Do.objects.all()
    response['todo'] = todo
    response['form'] = Status_Form
    return render(request, "LandingPage.html", response)

def doing_some(request):
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['status'] = request.POST['status']
        todo = Do(status=response['status'])
        todo.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def tespro(request):
    return render(request, "profile.html")

def jsonlibrary(request):
    books = list()
    base_url='https://www.googleapis.com/books/v1/volumes?q='
    query = 'quilting'
    if (request.GET):
        query = request.GET['query']
    response = requests.get(url=(base_url+query))
    data = response.json()
    try:
        for i in range(10):
            try:
                data['items'][i]['volumeInfo']['authors']
            except KeyError:
                data['items'][i]['volumeInfo']['authors'] = 'not found'
            books.append({
                'id':data['items'][i]['id'],
                'title':data['items'][i]['volumeInfo']['title'],
                'author':data['items'][i]['volumeInfo']['authors'][0],
                # 'publishedDate' : data['items'][i]['publishedDate'],
                'image':data['items'][i]['volumeInfo']['imageLinks']['thumbnail'],
            })
        return JsonResponse({'books':books})
    except KeyError:
        return JsonResponse({'books': ''})


def library(request):
    return render(request, 'book.html')

response={}
def subs(request):
    response['subs']=""
    if request.method =="POST":
        form = SubscriberForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            status = True
            try:
                Subscriber.objects.create(name=name, email=email, password=password)
            except:
                status = False
            return JsonResponse({'status':status})
    Form = SubscriberForm()
    response['form'] = Form
    return render(request, 'subscriber.html', response)

def getData(requests):
    users = Subscriber.objects.all().values('name', 'email', 'password')
    users_list = list(users)
    return JsonResponse(users_list, safe=False)

def unsubs(requests):
    id = requests.GET['password']
    Subscriber.objects.filter(password=id).delete()
    users = Subscriber.objects.all().values('name', 'password')
    userList = list(users)
    return JsonResponse(userList, safe=False)

def cekEmail(requests):
    if requests.method == "POST":
        email = requests.POST['email']
        validasi = Subscriber.objects.filter(pk=email).exists()
        return JsonResponse({'is_email': validasi})
def home(request):
    return render(request, 'home.html')
def login(request):
    return render(request, 'login.html')
def logout(request):
    auth.logout(request)
    request.session.flush()
    return HttpResponseRedirect('/book')

