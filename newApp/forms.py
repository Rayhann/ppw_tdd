from django import forms

class Status_Form(forms.Form):
	error_messages = {
		'required' : 'Please input this field',
	}
	mssg_status = {
		'type': 'text',
        'class': 'inputan',
        'placeholder':"What's on your mind"

	}
	status = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=mssg_status))

class SubscriberForm(forms.Form):
	name = forms.CharField(widget=forms.TextInput(attrs={'class':'input', 'placeholder': 'Masukan Nama', 'id':'name'}), max_length=100, label="Nama")
	email = forms.CharField(widget=forms.EmailInput(attrs={'class':'input', 'placeholder': 'Masukan Email', 'id':'email'}), max_length=100, label="Email")
	password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'input', 'placeholder':'Masukan Password', 'id':'password'}), max_length=100, label="Password")