from django.conf.urls import url, include
from .views import index, doing_some, tespro, library, jsonlibrary, subs, cekEmail, getData, unsubs, home, login, logout
    
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^doing_some', doing_some, name='doing_some'),
    url(r'^profile$', tespro, name='tespro'),
    url(r'^book$', library, name='book'),
    url(r'^jsonlibrary', jsonlibrary, name='jsonlibrary'),
    url(r'^subs$', subs, name='subs'),
    url(r'^check', cekEmail, name='cekEmail'),
    url(r'^listsubs', getData, name='getData'),
    url(r'^unsubs', unsubs, name='unsubs'),
    url(r'^login$', login, name='login'),
    url(r'^logout$', logout, name='logout'),
    url(r'^home$', home, name='home'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]
