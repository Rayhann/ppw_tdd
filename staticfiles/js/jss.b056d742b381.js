  var favorite = 0;

  function myFunction(x) {
    x.classList.toggle("far");
    x.classList.toggle("yellow");
     x.classList.toggle("fas");
     if($(x).hasClass("fas")){
      favorite++;
     }
     else {
      favorite--;
     }
    $('#id_favorite').html('Favorite : ' + favorite);

}

 $(document).ready(function($) {

        $('.accordion').find('.accordion-toggle').click(function(){

          //Expand or collapse this panel
          $(this).next().slideToggle('fast');

          //Hide the other panelsi
          $(".accordion-contain").not($(this).next()).slideUp('fast');

        });
        $("#cerah").click(function () {
	      $("body").css("background-color", "white");
	      $("#hh").css("color", "black");
	      $("#nn").css("color", "black");
	      $("#oo").css("color", "black");
	      $("#pp").css("color", "black");
	      $("#qq").css("color", "black");
	      $("#rr").css("color", "black");
	      $("#ss").css("color", "black");
	      $("#tt").css("color", "black");
	      $("#uu").css("color", "black");
      	});
      	$("#item").click(function () {
          $("body").css("background-color", "black");
       	  $("#hh").css("color", "white");
          $("#nn").css("color", "white");
          $("#oo").css("color", "white");
          $("#pp").css("color", "white");
          $("#qq").css("color", "white");
          $("#rr").css("color", "white");
          $("#ss").css("color", "white");
          $("#tt").css("color", "white");
          $("#uu").css("color", "white");
      });
      $.ajax("/jsonlibrary")
        .done(function (data) {
            data = data['books'];
            var tbody = '';
            for (var i = 0; i < data.length; i++) {
                tbody += '<tr id=\"'+ data[i].id +'\">'
                    +'<td><img class=\"img-thumbnail\" src=\"'+ data[i].image +'\"</td>'
                    +'<td>' + data[i].author + '</td>'
                    +'<td>' + data[i].title + '</td>'
                    +'<td>' + data[i].publishedDate+'</td>'
                    +'<td><i onclick="myFunction(this)" class="far fa-star"></i></td>';
            }
            $(tbody).appendTo('#id_table');
        });

    });
